//
//  ContentView.swift
//  ioasys_empresas
//
//  Created by Evens Taian on 05/12/19.
//  Copyright © 2019 Evens Taian. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @available(iOS 13.0.0, *)
    var body: some View {
        Text("Hello, World!")
    }
}

struct ContentView_Previews: PreviewProvider {
    @available(iOS 13.0.0, *)
    static var previews: some View {
        ContentView()
    }
}
