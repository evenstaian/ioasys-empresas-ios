//
//  EnterpriseViewController.swift
//  ioasys_empresas
//
//  Created by Evens Taian on 07/12/19.
//  Copyright © 2019 Evens Taian. All rights reserved.
//

import UIKit

class EnterpriseViewController: UIViewController {

    // MARK: Referencials
    var nome : String!
    var descricao : String!
    var img : String?
    
    @IBOutlet weak var ivEmpresa: UIImageView!
    @IBOutlet weak var lbDescricao: UILabel!
    
    // MARK: Init
    override func viewDidLoad() {
        putInfos()
        super.viewDidLoad()
    }
    
    // MARK: Handles
    func putInfos(){
        DispatchQueue.main.async{
            self.navigationItem.title = self.nome
            self.lbDescricao.text = self.descricao
        }
    }

}
