//
//  LoginViewController.swift
//  ioasys_empresas
//
//  Created by Evens Taian on 05/12/19.
//  Copyright © 2019 Evens Taian. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    // MARK: Referencials
    
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var contentViewBottom: NSLayoutConstraint!
    
    @IBOutlet weak var vwEntrarBottom: NSLayoutConstraint!
    
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfSenha: UITextField!
    
    @IBOutlet weak var lbBemVindo: UILabel!
    @IBOutlet weak var lbSubTitle: UILabel!
    
    
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var vwTitles: UIView!
    @IBOutlet weak var vwEntrar: UIView!
    @IBOutlet weak var btnEntrar: UIButton!
    

    // MARK: Init
    override func viewDidLoad() {
        
        putCloseKeyboardGesture()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardDidHideNotification, object: nil)
        super.viewDidLoad()
    }
    
    @objc
    func keyboardWillShow(notification: NSNotification) {
        
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {

            UIView.animate(withDuration: 0.3, animations: {
                self.vwEntrarBottom.constant = keyboardSize.height
            })
            
            lbBemVindo.isHidden = true
            lbSubTitle.isHidden = true
            
            self.view.layoutIfNeeded()
        
        }
    }
    
    @objc
    func keyboardWillHide(notification: NSNotification) {
        
        UIView.animate(withDuration: 0.2, animations: {
            self.vwEntrarBottom.constant = 0
            
            self.lbBemVindo.isHidden = false
            self.lbSubTitle.isHidden = false
            
            self.view.layoutIfNeeded()
        })
    
    }

    // MARK: Handles
    @IBAction func btnEntrar(_ sender: Any) {
        login()
    }
    
    func putCloseKeyboardGesture(){
        let tapGesture = UITapGestureRecognizer(target: self, action:#selector(closeKeyboard))
        contentView.addGestureRecognizer(tapGesture)
    }
    
    @objc
    func closeKeyboard(){
        tfEmail.resignFirstResponder()
        tfSenha.resignFirstResponder()
    }
    
    func inputsValid() -> Bool {
        if tfEmail.text! != "" && tfSenha.text! != "" {
            return true
        }else{
            return false
        }
    }
    
    func login(){
        DispatchQueue.main.async{
            self.showLoader(status: true)
        }
        
        if inputsValid(){
            let dados : LoginModelRequest = LoginModelRequest(email: tfEmail.text!, password: tfSenha.text!)
            
            Auth.Login(dadosLogin: dados) { (status, data) in
                do {
                    DispatchQueue.main.async{
                        self.showLoader(status: false)
                    }
                    
                    let res = try JSONDecoder().decode(LoginModelResponse.self , from: data)
                    print(res)
                    
                    if status == 200 {
                        DispatchQueue.main.async{
                            self.performSegue(withIdentifier: "segueLogin", sender: nil)
                        }
                    }else{
                        self.showAlerta(titulo: "Ocorreu um erro :(", mensagem: "Parece que seu email ou a senha não estão corretos. Verifica e tenta novamente, vai?")
                        print(res.errors![0])
                    }
                }catch{
                    print(error)
                }
                
            }
        }else{
            self.showAlerta(titulo: "Atencão!", mensagem: "Preencha todos os campos")
        }
        
    }
    
    func showLoader(status : Bool){
        let loader = self.storyboard?.instantiateViewController(withIdentifier: "loader") as! LoaderViewController
        if(status){
            present(loader, animated: false, completion: nil)
        }else{
            self.dismiss(animated: false, completion: nil)
        }
    }

}
