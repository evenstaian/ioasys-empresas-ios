//
//  SearchEnterprisesViewController.swift
//  ioasys_empresas
//
//  Created by Evens Taian on 05/12/19.
//  Copyright © 2019 Evens Taian. All rights reserved.
//

import UIKit

class SearchEnterprisesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {

    // MARK: Referencials
    var enterprises_list : [Enterprise] = []
    
    var nome_empresa : String!
    var descricao_empresa : String!
    var img_empresa : String!
    
    let searchController = UISearchController(searchResultsController: nil)
    
    var activityIndicator : UIActivityIndicatorView = UIActivityIndicatorView()
    
    @IBOutlet weak var enterprisesTableView: UITableView!
    @IBOutlet weak var vwEnterprises: UIView!
    @IBOutlet weak var vwInfos: UIView!
    @IBOutlet weak var lbInfos: UILabel!
    @IBOutlet weak var vwLoader: UIView!
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueEmpresa"{
            let vcEmpresa = segue.destination as! EnterpriseViewController
            vcEmpresa.nome = nome_empresa
            vcEmpresa.descricao = descricao_empresa
            vcEmpresa.img = img_empresa
        }
    }
    
    // MARK: Init
    override func viewDidLoad() {
        showEnterprisesView(status: false, msg_error: "Clique na busca para iniciar")
        addImageLogoOnNavController()
        addSearchButton()
        addIndicator()
        super.viewDidLoad()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        enterprises_list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "enterprisesCell", for: indexPath) as! EnterprisesTableViewCell
        
        cell.lbName.text = enterprises_list[ indexPath.row ].enterprise_name
        cell.lbType.text = enterprises_list[ indexPath.row ].enterprise_type?[0].enterprise_type_name
        cell.lbCountry.text = enterprises_list[ indexPath.row ].country
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.nome_empresa = enterprises_list[ indexPath.row ].enterprise_name
        self.descricao_empresa = enterprises_list[ indexPath.row ].description
        self.img_empresa = enterprises_list[ indexPath.row ].photo
        
        DispatchQueue.main.async{
            self.performSegue(withIdentifier: "segueEmpresa", sender: nil)
            
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print(searchText)
        if searchText == "" {
            showEnterprisesView(status: false, msg_error: "Clique na busca para iniciar")
        }else{
            searchEnterprise(name: searchText)
        }
    }
    
    // MARK: Handles
    func addImageLogoOnNavController(){
        
        let navController = navigationController!
        
        let navigationBar = navController.navigationBar
        
        let image = UIImage(named: "logoHome")
        
        let bannerWidth = navController.navigationBar.frame.size.width
        let bannerHeight = navController.navigationBar.frame.size.height
        let bannerX = bannerWidth / 2 - (image?.size.width)! / 3
        let bannerY = bannerHeight * 2 - (image?.size.height)! * 2
        
        let imageView = UIImageView(frame: CGRect(x: bannerX, y: bannerY, width: bannerWidth/4, height: bannerHeight/4))
        imageView.contentMode = .scaleAspectFill
        imageView.image = image?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = .white
        
        navigationBar.addSubview(imageView)
        
    }
    
    func addSearchButton(){
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "icSearch"), style: .plain, target: self, action: #selector(openSearch))
        self.navigationItem.rightBarButtonItem?.tintColor = .white
    }
    
    func addIndicator(){
        activityIndicator.center = self.view.center
        activityIndicator.style = UIActivityIndicatorView.Style.gray
        activityIndicator.hidesWhenStopped = true
        self.view.addSubview(activityIndicator)
    }
    
    @objc func openSearch() {
        self.searchController.hidesNavigationBarDuringPresentation = false
        self.searchController.searchBar.delegate = self
        self.searchController.searchBar.barTintColor = UIColor(red:0.87, green:0.28, blue:0.45, alpha:1.0)
        self.searchController.searchBar.tintColor = .white
        self.searchController.searchBar.searchTextField.backgroundColor = .white
        self.searchController.searchBar.placeholder = "Pesquise a empresa por nome"
        
        DispatchQueue.main.async{
            self.present(self.searchController, animated: true, completion: nil)
        }
    }
    
    func searchEnterprise(name: String){
        
        DispatchQueue.main.async{
            self.showLoader(status: true)
        }
        
        Requests.GetEnterprisesWithIndex(name: name) { (status, data) in
            do {
                
                DispatchQueue.main.async{
                    self.showLoader(status: false)
                }
                
                let res = try JSONDecoder().decode(EnterpriseModelResponse.self , from: data)
                print(res)
                
                if status == 200 {
                    self.enterprises_list = res.enterprises!
                    
                    if !self.enterprises_list.isEmpty{
                        DispatchQueue.main.async{
                            self.enterprisesTableView.reloadData()
                        }
                        self.showEnterprisesView(status: true, msg_error: "")
                    }else{
                        self.showEnterprisesView(status: false, msg_error: "Não há empresas com esse nome" )
                    }
                                        
                }else{
                    
                    self.showEnterprisesView(status: false, msg_error: "Ocorreu um erro")

                    if status == 401 {
                        //Logout
                        print("LOGOUT")
                    }

                    print(res.errors!)
                }
            }catch{
                print(error)
            }
        }
        
    }
    
    func showEnterprisesView(status : Bool, msg_error : String){
        DispatchQueue.main.async{
            self.vwEnterprises.isHidden = !status
            self.vwInfos.isHidden = status
            self.lbInfos.text = msg_error
        }
    }
    
    func showLoader(status : Bool){
        if status {
            activityIndicator.startAnimating()
        }else{
            activityIndicator.stopAnimating()
        }
    }

}
