//
//  EnterprisesTableViewCell.swift
//  ioasys_empresas
//
//  Created by Evens Taian on 05/12/19.
//  Copyright © 2019 Evens Taian. All rights reserved.
//

import UIKit

class EnterprisesTableViewCell: UITableViewCell {

    // MARK: Referencials
    @IBOutlet weak var ivEnterprise: UIImageView!
    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var lbType: UILabel!
    @IBOutlet weak var lbCountry: UILabel!

}
