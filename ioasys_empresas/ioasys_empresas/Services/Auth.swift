//
//  Auth.swift
//  ioasys_empresas
//
//  Created by Evens Taian on 05/12/19.
//  Copyright © 2019 Evens Taian. All rights reserved.
//

import Foundation

//BaseURL
private let url = URL(string: Constants.BASE_URL)

//Endpoints
private let login = "users/auth/sign_in"

//Configs
private let configuration: URLSessionConfiguration = {
    let config = URLSessionConfiguration.default
    config.httpAdditionalHeaders = [ "Content-Type" : "application/json"]
    return config
}()

// Session
private let session = URLSession(configuration: configuration)

class Auth {
    
    class func Login(dadosLogin: LoginModelRequest, onComplete: @escaping (Int, Data) -> Void){
        
        guard let end_login = URL(string: login, relativeTo: url) else {return}
        var request = URLRequest(url: end_login)
        request.httpMethod = "POST"
        
        guard let json = try? JSONEncoder().encode(dadosLogin) else {return}
        request.httpBody = json
        
        let dataTask = session.dataTask(with: request) { (data, response, error) in
            if error == nil {
                let httpResponse = response as? HTTPURLResponse
                
                if(httpResponse!.statusCode == 401){
                    onComplete(httpResponse!.statusCode, data!)
                }else{
                    guard let uid = httpResponse!.allHeaderFields["uid"] as? String else { return }
                    guard let client = httpResponse!.allHeaderFields["client"] as? String else { return }
                    guard let token = httpResponse!.allHeaderFields["access-token"] as? String else { return }
                    
                    DispatchQueue.main.async{
                        if Storage.apagarUsuario(){
                            print("DADOS ANTERIORES APAGADOS COM SUCESSO")
                        }else{
                            print("FALHA AO APAGAR DADOS")
                        }
                    }
                    
                    DispatchQueue.main.async{
                        if Storage.saveUsuario(token: token, client: client, uid: uid){
                            print("DADOS INCLUÍDOS COM SUCESSO")
                        }else{
                            print("FALHA AO INCLUIR DADOS")
                        }
                    }
                    
                    print(httpResponse!.statusCode, uid, client, token)
                    
                    onComplete(httpResponse!.statusCode, data!)
                }
                
            }else{
                print(error!)
            }
        }
        dataTask.resume()
    }
    
}
