//
//  LoginModelRequest.swift
//  ioasys_empresas
//
//  Created by Evens Taian on 05/12/19.
//  Copyright © 2019 Evens Taian. All rights reserved.
//

struct LoginModelRequest : Codable {
    
    var email : String
    var password : String
    
}
