//
//  EnterpriseModelResponse.swift
//  ioasys_empresas
//
//  Created by Evens Taian on 05/12/19.
//  Copyright © 2019 Evens Taian. All rights reserved.
//

struct EnterpriseModelResponse : Codable {
    var enterprises : [ Enterprise ]?
    var errors : Int?

}

struct Enterprise : Codable {
    var id : Int?
    var enterprise_name : String?
    var description : String?
    var photo : String?
    var enterprise_type : [EnterpriseType]?
    var country : String?
    var success : Bool?
}

struct EnterpriseType : Codable {
    var id : Int
    var enterprise_type_name : String
}
