//
//  LoginModelResponse.swift
//  ioasys_empresas
//
//  Created by Evens Taian on 05/12/19.
//  Copyright © 2019 Evens Taian. All rights reserved.
//

struct LoginModelResponse : Codable {
    //var investor : [Investor]?
    var enterprise : String?
    var success : Bool?
    var errors : [String]?
}

struct Investor : Codable {
    var id : Int?
    //var investor_name : String?
    //var email : String?
    //var city : String?
    //var country : String?
    //var photo : String?
}
