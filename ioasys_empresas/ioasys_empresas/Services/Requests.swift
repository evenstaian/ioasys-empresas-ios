//
//  Auth.swift
//  ioasys_empresas
//
//  Created by Evens Taian on 05/12/19.
//  Copyright © 2019 Evens Taian. All rights reserved.
//

import Foundation

//BaseURL
private let url = URL(string: Constants.BASE_URL)

//Endpoints
private let enterprises = "enterprises"

//Configs
private func configuration() -> URLSessionConfiguration {
    
    let usuario = Storage.getUsuario()

    let token = usuario?.first?.token ?? "0"
    let client = usuario?.first?.client ?? "0"
    let uid = usuario?.first?.uid ?? "0"
    
    let config = URLSessionConfiguration.default
    config.httpAdditionalHeaders = [
        "Content-Type" : "application/json",
        "access-token" : token,
        "client" : client,
        "uid" : uid ]
    return config
}

// Session
private func getSession() -> URLSession{
    return URLSession(configuration: configuration())
}

class Requests {

    class func GetEnterprisesWithIndex(name: String, onComplete: @escaping (Int, Data) -> Void){
        print("REQUESTS - REQUISITAR EMPRESAS")
        
        guard let end_enterprises = URL(string: "\(enterprises)?enterprise_types=1&name\(name)", relativeTo: url) else {return}
        var request = URLRequest(url: end_enterprises)
        print(end_enterprises)
        request.httpMethod = "GET"
        
        let dataTask = getSession().dataTask(with: request) { (data, response, error) in
            if error == nil {
                let httpResponse = response as? HTTPURLResponse

                print("REQUESTS - RESPOSTA:", String(decoding: data!, as: UTF8.self))   
                onComplete(httpResponse!.statusCode, data!)
                
            }else{
                print(error!)
            }
        }
        dataTask.resume()
    }

}
