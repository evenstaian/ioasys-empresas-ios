//
//  Alerts.swift
//  ioasys_empresas
//
//  Created by Evens Taian on 07/12/19.
//  Copyright © 2019 Evens Taian. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    func showAlerta(titulo: String, mensagem: String){
        
        let alerta = UIAlertController(title: titulo, message: mensagem, preferredStyle: .alert)
        let dismiss = UIAlertAction(title: "Ok, entendi", style: .cancel, handler: nil)
        
        alerta.addAction(dismiss)
        present(alerta, animated: true, completion: nil)
    }
    
}
