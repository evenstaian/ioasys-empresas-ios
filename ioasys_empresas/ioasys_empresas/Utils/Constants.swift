//
//  Constants.swift
//  ioasys_empresas
//
//  Created by Evens Taian on 05/12/19.
//  Copyright © 2019 Evens Taian. All rights reserved.
//

public class Constants {
    
    public static let BASE_URL = "https://empresas.ioasys.com.br/api/v1/"
    
}
