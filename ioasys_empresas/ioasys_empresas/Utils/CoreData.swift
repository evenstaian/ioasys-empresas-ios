//
//  CoreData.swift
//  ioasys_empresas
//
//  Created by Evens Taian on 07/12/19.
//  Copyright © 2019 Evens Taian. All rights reserved.
//

import UIKit
import CoreData

class Storage {
    
    private class func getContext() -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    
    // MARK: - INSERT
    
    class func saveUsuario(token: String, client: String, uid: String) -> Bool{
        
        let context = getContext()
        
        let entity = NSEntityDescription.entity(forEntityName: "User", in: context)
        
        let manageObject = NSManagedObject(entity: entity!, insertInto: context)
        
        manageObject.setValue(token, forKey: "token")
        manageObject.setValue(uid, forKey: "uid")
        manageObject.setValue(client, forKey: "client")
        
        do {
            try context.save()
            return true
        }catch {
            return false
        }
    }
    
    // MARK: - GET
    
    class func getUsuario() -> [User]? {
        let context = getContext()
        var usuarioDoBanco: [User]?
        
        do {
            usuarioDoBanco = try context.fetch(User.fetchRequest())
            return usuarioDoBanco
        }catch {
            return usuarioDoBanco
        }
    }
    
    // MARK: - DELETE
    
    class func apagarUsuario() -> Bool {
        let context = getContext()
        let delete = NSBatchDeleteRequest(fetchRequest: User.fetchRequest())
        
        do {
            try context.execute(delete)
            return true
        }catch {
            return false
        }
    }
}
