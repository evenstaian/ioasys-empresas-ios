//
//  LoaderViewController.swift
//  ioasys_empresas
//
//  Created by Evens Taian on 07/12/19.
//  Copyright © 2019 Evens Taian. All rights reserved.
//

import UIKit

class LoaderViewController: UIViewController {
    
    // MARK: Referencials
    var activityIndicator : UIActivityIndicatorView = UIActivityIndicatorView()

    // MARK: Init
    override func viewDidLoad() {
        setIndicator()
        super.viewDidLoad()
    }
    
    // MARK: Handles
    func setIndicator(){
        activityIndicator.center = self.view.center
        activityIndicator.style = UIActivityIndicatorView.Style.white
        self.view.addSubview(activityIndicator)
        
        activityIndicator.startAnimating()
    }

}
